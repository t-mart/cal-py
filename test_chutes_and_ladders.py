from chutes_and_ladders import Board

import pytest

def verify_board(board_list, expected):
    b = Board(board_list)
    actual = b.dist_away()
    print(actual)
    if actual != False:
        actual = [m.with_roll_of for m in actual[1:]]
    assert expected == actual

def test_boards():
    verify_board([2,2,-1], [4])
    verify_board([ 4, 0, -2, 0, 1, 1, 1, 1, 1 ], [4, 6])
    verify_board([0,-1,-2,-3,-4,-5,-6], False)
    verify_board([ 0, -1, -2, -3, -4, -5 ], [1, 6])
    verify_board([ 4, 0, 0, 0, -1, 0, 0, 0 ], [6, 3])
    verify_board([ 10, 10, 10, 10, 10, 10], False)
