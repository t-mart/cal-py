from array import array
from collections import namedtuple

MAX_DICE_ROLL = 6

BestMove = namedtuple('BestMove', ['turn', 'comingfrom', 'with_roll_of'])

def backtrack_best_moves(best_moves):
    tracks = []
    move = best_moves[-1]
    if not move:
        return False
    tracks.insert(0, move)
    while move.comingfrom != None:
        move = best_moves[move.comingfrom]
        tracks.insert(0, move)
    return tracks

class Board(array):
    def __new__(cls, initializer):
        a =  array.__new__(cls, 'i', initializer)
        a.insert(0, 0) #include the last spot
        a.append(0) #include the last spot
        return a

    def dist_away(self):
        best_moves = [None for i in self]
        best_moves[0] = BestMove(0, None, None)
        indicies_to_process = [0]
        count = 0

        while indicies_to_process:
            count += 1
            index = indicies_to_process.pop()
            tile = self[index]
            cur_best_move = best_moves[index]
            if tile != 0:
                next_tile = index + tile
                if not (0 <= next_tile < len(self)):
                    return False
                offsets = [tile]
            else:
                max_roll = min(MAX_DICE_ROLL, len(self) - index - 1)
                offsets = range(1, max_roll + 1)
            for offset in offsets:
                jump = index + offset
                if (best_moves[jump] and best_moves[jump].turn > (cur_best_move.turn + 1)):
                    best_moves[jump] = BestMove(cur_best_move.turn + 1, index, offset)
                elif not best_moves[jump]:
                    best_moves[jump] = BestMove(cur_best_move.turn + 1, index, offset)
                    if jump != len(self) - 1:
                        indicies_to_process.append(jump)
        print("%d iterations" % (count))
        return backtrack_best_moves(best_moves)

